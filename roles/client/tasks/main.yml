# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

---
- name: Convert hostname to innernet peer name
  # we want the mere host name before the domain, so e.g.
  # * server1.fsfe.org -> server1
  # * cont1.noris.fsfeurope.org -> cont1-noris
  set_fact:
    innernet_client: "{{ innernet_client | replace(item.0, item.1) }}"
  vars:
    - innernet_client: "{{ ansible_host }}"
  loop:
    - ['.', '-']
    - ['-fsfeurope-org', '']
    - ['-fsfe-org', '']
    - ['-fsfe-be', '']

- name: Gather which packages are installed on the client
  tags: [update, uninstall]
  package_facts:
    manager: auto

- name: Make sure needed packages for innernet and wireguard are installed
  apt:
    package:
      - python3-pexpect
      - rsync
      - wireguard
      - wireguard-tools
      - ufw

- name: Remove existing innernet configuration on client
  tags: [never, uninstall]
  expect:
    command: "innernet uninstall {{ network_name }}"
    responses:
      (?i)delete: "yes"
  when: "'innernet' in ansible_facts.packages"

- name: Remove innernet package on client
  tags: [never, uninstall]
  apt:
    name: innernet
    state: absent
    purge: yes
  when: "'innernet' in ansible_facts.packages"

- name: Install innernet package on client
  tags: [update]
  block:
    - name: Copy innernet package to client
      synchronize:
        src: "innernet.deb"
        dest: "/tmp/innernet.deb"

    - name: Install innernet client package
      apt:
        deb: "/tmp/innernet.deb"
        update_cache: true
        install_recommends: true
  # If 1. innernet not installed or 2. `update` tag executed
  when: "'innernet' not in ansible_facts.packages or 'update' in ansible_run_tags"

- name: Get existing peers from innernet-server database
  shell: 'sqlite3 /var/lib/innernet-server/{{ network_name }}.db "select name from peers;"'
  register: existing_peers
  delegate_to: "{{ innernet_server }}"
  run_once: true

- name: Add machine as innernet peer
  include_role:
    name: server
    tasks_from: add_peer
  args:
    apply:
      delegate_to: "{{ innernet_server }}"
  vars:
    peer_name: "{{ innernet_client }}"
    # Value of the CIDR we defined as the CIDR for machines
    peer_cidr: "{{ machine_cidr }}"
    # machines are never admins
    peer_admin: "false"
  when:
    - innernet_client not in existing_peers.stdout_lines

- name: Install innernet peer invitation on machine
  block:
    - name: Copy peer invitation file from controller to client
      copy:
        src: "{{ innernet_client }}.toml"
        dest: "/root/{{ innernet_client }}.toml"

    - name: Install peer invitation on client
      shell: |
        innernet install /root/{{ innernet_client }}.toml \
          --default-name \
          --delete-invite
  when:
    - innernet_client not in existing_peers.stdout_lines

- name: Set listen port on client
  tags: [listen_port]
  shell: |
        innernet set-listen-port {{ network_name }} \
          -l {{ network_listen_port_clients }} \
          --yes
  register: listen_port_changed
  changed_when: "'No change necessary' not in listen_port_changed.stdout"

- name: Allow UDP traffic on WireGuard port
  tags: [listen_port]
  ufw:
    to_port: "{{ network_listen_port_clients }}"
    rule: allow
    proto: udp

- name: Restart and enable innernet daemon
  tags: [update, listen_port]
  systemd:
    name: "innernet@{{ network_name }}"
    state: restarted
    enabled: yes
    daemon_reload: yes
