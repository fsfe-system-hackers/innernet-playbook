FROM rust:slim-bullseye

RUN cargo install cargo-deb
RUN apt-get update
RUN apt-get install -y libsqlite3-dev libclang-dev
RUN mkdir -p /innernet
WORKDIR /innernet
