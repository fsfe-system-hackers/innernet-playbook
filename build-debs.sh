#!/usr/bin/env bash

# build needed Docker container
docker build . -t rust-deb:bullseye

# Go to innernet-src submodule
cd innernet-src || exit

INNERNET_VERSION="${INNERNET_VERSION:-1.6.0}"

# Checkout correct version
git checkout "v${INNERNET_VERSION}"

# Build binaries
echo "Building binaries"
docker run --rm -v $PWD:/innernet rust-deb:bullseye cargo deb -p client
docker run --rm -v $PWD:/innernet rust-deb:bullseye cargo deb -p server

# Copy binaries to Ansible roles
echo "Copying binaries"
mkdir -p ../roles/server/files && \
    cp target/debian/innernet-server_"$INNERNET_VERSION"_amd64.deb ../roles/server/files/innernet-server.deb

mkdir -p ../roles/client/files && \
    cp target/debian/innernet_"$INNERNET_VERSION"_amd64.deb ../roles/client/files/innernet.deb
