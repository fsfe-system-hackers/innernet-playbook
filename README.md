[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/innernet-playbook/00_README)

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

**Table of Contents**

- [Motivation](#motivation)
- [Install](#install)
  - [Clone the repo](#clone-the-repo)
  - [Build packages from submodule at `./innernet-src`](#build-binaries-from-submodule-at-innernet-src)
  - [Run the playbook](#run-the-playbook)

<!-- markdown-toc end -->

# Motivation

There is a need for some of our servers to connect to other IPv6-only hosts. Since this is not always possible without introducing major painpoints elsewhere, we simply create an internal WireGuard network so that the machines in question can communicate securely using IPv4.

![An overview](fsfe-innernet.png)

You can learn more about innernet by looking at its [source code](./innernet-src) or reading this informative [blog post](https://blog.tonari.no/introducing-innernet) of its creator.

# Preparation

## Requirements

* A somewhat recent version of `ansible`
* `git`

## Clone the repo

```bash
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/innernet-playbook.git
cd innernet-playbook
```

# Deployment

In the scope of this playbook and its roles, we have three different categories of computers:

1. The innernet server, being the central connector of all innernet peers
2. Automatically managed machines that are innernet peers, mostly VMs
3. Manually managed peers, for example admins and other humans

## Configure server and all clients

Run the whole playbook to configure everything. For the innernet server and
automatically managed machines, all will be handled. For the manually managed
peers, you will be given an invitation file.

```bash
ansible-playbook deploy.yml
```

## Add a new machine

In order to add e.g. a virtual machine to the networks, run these steps:

1. In the inventory, add the `innernet_client` group to the host
2. Run the playbook with `ansible-playbook -l newserver.org deploy.yml`

This will configure both the necessary parts on the server and the new machine.

## Add a new manually managed peer

In order to add a new human or otherwise manually managed innernet peer, run
these steps:

1. In `all.yml`, add a new entry for `manual_peers`
2. Run the playbook with `ansible-playbook -l innernet_server deploy.yml`
3. Install innernet and import the invitation file on the new peer's computer
   (see below). They are in `roles/client/files/` then.

### Distribute the invitation files

Some invitation files are for humans, so you need to send these files to them
securely. We suggest using something like `wormohle`.

```bash
sudo apt install magic-wormhole
cd roles/client/files
wormhole send <name_of_peer>.toml
```

# Update

Since [innernet](https://github.com/tonarino/innernet) is new software, it is
not yet included in the Debian repositories. Thus, before running the playbook
we need to build the `innernet` and `innernet-server` binaries.

In order to switch to a newer version of innernet, run the following steps:

1. Check out the desired tag in the `innernet-src` submodule
2. Run the build script: `./build-debs.sh`
3. Run the playbook with `ansible-playbook -t update deploy.yml`

# Associations

The different CIDRs can have [associations](https://github.com/tonarino/innernet#adding-associations-between-cidrs), e.g. so that admins can access
machines, although they are not in the same subnet.

These have to be configure by an admin!

Currently, the `admins` CIDR is associated with all other CIDRs (i.e. `humans` >
`others` and `machines`).

# Ansible tags

Some tags allow you to specify just certain operations. Here are the currently
available ones:

* `cidr`: configure CIDRs
* `update`: update the innernet binaries
* `listen_port`: edit/set the listen port between server and clients
* `uninstall`: delete innernet configuration and packages from systems
