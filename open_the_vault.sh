#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -euo pipefail

gpg --batch --use-agent --decrypt vault_passphrase.gpg
